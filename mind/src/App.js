import React from 'react';
import logo from './logo.svg';
import './App.css';
import Personal from './component/Personal';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div >
      <Personal/>
    </div>
  );
}

export default App;
