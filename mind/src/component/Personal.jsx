import React, { Component } from 'react';
import {Container,Navbar,Form,Button} from 'react-bootstrap';

export default  class Personal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name:'',
            age:'',
            student:'',
            teacher:'',
            developer:''
        
        };

        this.handleNameChange=this.handleNameChange.bind(this);
        this.handleAgeChange=this.handleAgeChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleCheck=this.handleCheck.bind(this);
        this.handleCheck1=this.handleCheck1.bind(this);
        this.handleCheck2=this.handleCheck2.bind(this);

    }
        handleNameChange = event => {
            this.setState({
                name:event.target.value
            })
        }
    handleAgeChange = event =>{
            this.setState({
                age:event.target.value
            })
    }
    handleCheck = event =>{
        this.setState({
            student:event.target.checked
        })
    }
    handleCheck1 = event =>{
        this.setState({
            student:event.target.checked
        })
    }
    handleCheck2 = event =>{
        this.setState({
            student:event.target.checked
        })
    }

    handleSubmit = event =>{
        alert(`${this.state.name} ${this.state.age} ${this.state.student} `)
        event.preventDefault();
    }
   
       
    
    render () {

        return (
            <div>
                 {/* <Container>
                    <Navbar expand="lg" variant="dark" bg="dark">
                        <Navbar.Brand   href="#"><h2 style={Textstyle}>Personal Information</h2> </Navbar.Brand>
                    </Navbar>
                </Container> */}
        <div style={{width:'80%', margin:'auto'}}>
        <div style={{backgroundColor:"blue"}}>
                    <h1  style={{textAlign:"center",padding:'12px',color:"white"}} >Personal Information</h1>
                </div>
        </div>
               

              <Form onSubmit={this.handleSubmit}>
                  
              <div className="container">

                        <div className="row">

                            <div className="col-md-6">

                            <Form.Group >

                                    <br />
                                    <Form.Label>Name:</Form.Label>
                                    <Form.Control size="lg" type="text" value={this.state.name} 	onChange={this.handleNameChange} placeholder="Input name" />
                                    <br />
                                    <Form.Label>Age:</Form.Label>
                                    <Form.Control size="lg" type="text" value={this.state.age} 	onChange={this.handleAgeChange} placeholder="Input age" />
                                    <br />


                            </Form.Group>

                            </div>

                            <div className="col-md-6">
                                <Form.Group>

                                    <br/>
                                <Form.Label>Gender:</Form.Label>

                                <div className="radio">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-ml-3">
                                            <label>
                                            <input type="radio" value="option1"  />&nbsp;Male
                                            
                                        </label>
                                            </div>
                                            &nbsp;&nbsp;
                                            <div className="col-ml-3">
                                            <label>
                                            <input type="radio" value="option2"  />&nbsp;Female
                                            
                                        </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                    <br/>
                                    <Form.Label>Job:</Form.Label>
                                        <div className="checkbox">
                                    
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-ml3">

                                                    <label>
                                                        <input type="checkbox" 
                                                        checked={this.state.student}
                                                        onChange={this.handleCheck} /> Student 
                                                            
                                                    </label>

                                                    </div>
                                                    &nbsp;&nbsp;
                                                    <div className="col-ml3">
                                                    <label>

                                                        <input type="checkbox"
                                                         checked={this.state.teacher}
                                                         onChange={this.handleCheck1}/> Teacher
                                                
                                                    </label>
                                                    </div>
                                                    &nbsp;&nbsp;
                                                    <div className="col-ml3">
                                                    <label>
                                                    <input type="checkbox" 
                                                     checked={this.state.developer}
                                                     onChange={this.handleCheck2} /> Developer
                                                
                                                    </label>
                                                    </div>
                                                </div>
                                    
                                    </div>
                                </div>  

                                </Form.Group>
                        

                            </div>

                        </div>

                        <Button variant="primary" type="submit">  Submit  </Button>

                            <br/>
                            <Container style={{  alignItems:'center',justifyContent:'center'}}>
                            <div  className="row" >

                                <br/>
                                <div className="col-ml-3">
                                <h5>Display data:</h5>

                                </div>    
                                &nbsp;
                                <div className="col-ml-3">


                                <Button variant="outline-secondary">List</Button>  <Button variant="outline-secondary">Card</Button> {' '}

                                </div>          

                                </div>

                            </Container>
                       
                        </div>



              </Form>  
               
            </div>
        )
    }
}



const Textstyle={
    textAlign: "center",
    
    
};